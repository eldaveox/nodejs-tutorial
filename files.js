const fs = require('fs'); //fs = file system


//reading files

/*fs.readFile('./docs/text.txt', (err,data) => {
    if (err) {
        console.log(err);
    }
    console.log(data.toString());
});

console.log('this is the last line of code');*/

//writing files

/*fs.writeFile('./docs/text2.txt', 'Hello World 2!', () => {
    console.log('file was written again');
});*/

//directories

/*if (!fs.existsSync('./assets')) {
    fs.mkdir('./assets', (err) => {
        if (err) {
            console.log(err);
        }
        console.log('folder created');
    });
} else {
    fs.rmdir('./assets', (err) => {
        if (err) {
            console.log(err)
        };
        console.log('folder deleted');
    })
}*/

//deleting files

if (fs.existsSync('./docs/deleteme.txt')) {
    fs.unlink('./docs/deleteme.txt', (err) => {
        if (err) {
            console.log(err);
        }
        console.log('file deleted');
    })
}
